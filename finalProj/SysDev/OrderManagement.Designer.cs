﻿namespace SysDev
{
    partial class OrderManagement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LogoImage = new System.Windows.Forms.PictureBox();
            this.cornerUsernamelabel = new System.Windows.Forms.Label();
            this.backToHomePageButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.updatePaymentButton = new System.Windows.Forms.Button();
            this.OrderManagementLabel = new System.Windows.Forms.Label();
            this.bookingIDLabel = new System.Windows.Forms.Label();
            this.totalPriceLabel = new System.Windows.Forms.Label();
            this.amountPaidLabel = new System.Windows.Forms.Label();
            this.amountPendingLabel = new System.Windows.Forms.Label();
            this.emailLabel = new System.Windows.Forms.Label();
            this.orderManagementListBox = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.LogoImage)).BeginInit();
            this.SuspendLayout();
            // 
            // LogoImage
            // 
            this.LogoImage.Image = global::SysDev.Properties.Resources.unnamed;
            this.LogoImage.Location = new System.Drawing.Point(211, 8);
            this.LogoImage.Margin = new System.Windows.Forms.Padding(2);
            this.LogoImage.Name = "LogoImage";
            this.LogoImage.Size = new System.Drawing.Size(103, 65);
            this.LogoImage.TabIndex = 3;
            this.LogoImage.TabStop = false;
            // 
            // cornerUsernamelabel
            // 
            this.cornerUsernamelabel.AutoSize = true;
            this.cornerUsernamelabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cornerUsernamelabel.Location = new System.Drawing.Point(8, 6);
            this.cornerUsernamelabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.cornerUsernamelabel.Name = "cornerUsernamelabel";
            this.cornerUsernamelabel.Size = new System.Drawing.Size(81, 17);
            this.cornerUsernamelabel.TabIndex = 4;
            this.cornerUsernamelabel.Text = "Username";
            // 
            // backToHomePageButton
            // 
            this.backToHomePageButton.Location = new System.Drawing.Point(11, 42);
            this.backToHomePageButton.Margin = new System.Windows.Forms.Padding(2);
            this.backToHomePageButton.Name = "backToHomePageButton";
            this.backToHomePageButton.Size = new System.Drawing.Size(70, 35);
            this.backToHomePageButton.TabIndex = 7;
            this.backToHomePageButton.Text = "Back to Home page";
            this.backToHomePageButton.UseVisualStyleBackColor = true;
            this.backToHomePageButton.Click += new System.EventHandler(this.backToHomePageButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(-157, 125);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(962, 17);
            this.label1.TabIndex = 8;
            this.label1.Text = "_________________________________________________________________________________" +
    "_________________________";
            // 
            // updatePaymentButton
            // 
            this.updatePaymentButton.Location = new System.Drawing.Point(455, 90);
            this.updatePaymentButton.Margin = new System.Windows.Forms.Padding(2);
            this.updatePaymentButton.Name = "updatePaymentButton";
            this.updatePaymentButton.Size = new System.Drawing.Size(70, 35);
            this.updatePaymentButton.TabIndex = 9;
            this.updatePaymentButton.Text = "Update Payment";
            this.updatePaymentButton.UseVisualStyleBackColor = true;
            this.updatePaymentButton.Click += new System.EventHandler(this.updatePaymentButton_Click);
            // 
            // OrderManagementLabel
            // 
            this.OrderManagementLabel.AutoSize = true;
            this.OrderManagementLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OrderManagementLabel.Location = new System.Drawing.Point(8, 109);
            this.OrderManagementLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.OrderManagementLabel.Name = "OrderManagementLabel";
            this.OrderManagementLabel.Size = new System.Drawing.Size(147, 17);
            this.OrderManagementLabel.TabIndex = 10;
            this.OrderManagementLabel.Text = "Order Management";
            // 
            // bookingIDLabel
            // 
            this.bookingIDLabel.AutoSize = true;
            this.bookingIDLabel.Location = new System.Drawing.Point(11, 150);
            this.bookingIDLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.bookingIDLabel.Name = "bookingIDLabel";
            this.bookingIDLabel.Size = new System.Drawing.Size(60, 13);
            this.bookingIDLabel.TabIndex = 11;
            this.bookingIDLabel.Text = "Booking ID";
            // 
            // totalPriceLabel
            // 
            this.totalPriceLabel.AutoSize = true;
            this.totalPriceLabel.Location = new System.Drawing.Point(117, 150);
            this.totalPriceLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.totalPriceLabel.Name = "totalPriceLabel";
            this.totalPriceLabel.Size = new System.Drawing.Size(58, 13);
            this.totalPriceLabel.TabIndex = 12;
            this.totalPriceLabel.Text = "Total Price";
            // 
            // amountPaidLabel
            // 
            this.amountPaidLabel.AutoSize = true;
            this.amountPaidLabel.Location = new System.Drawing.Point(209, 150);
            this.amountPaidLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.amountPaidLabel.Name = "amountPaidLabel";
            this.amountPaidLabel.Size = new System.Drawing.Size(67, 13);
            this.amountPaidLabel.TabIndex = 13;
            this.amountPaidLabel.Text = "Amount Paid";
            // 
            // amountPendingLabel
            // 
            this.amountPendingLabel.AutoSize = true;
            this.amountPendingLabel.Location = new System.Drawing.Point(309, 150);
            this.amountPendingLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.amountPendingLabel.Name = "amountPendingLabel";
            this.amountPendingLabel.Size = new System.Drawing.Size(85, 13);
            this.amountPendingLabel.TabIndex = 14;
            this.amountPendingLabel.Text = "Amount Pending";
            // 
            // emailLabel
            // 
            this.emailLabel.AutoSize = true;
            this.emailLabel.Location = new System.Drawing.Point(419, 150);
            this.emailLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.emailLabel.Name = "emailLabel";
            this.emailLabel.Size = new System.Drawing.Size(32, 13);
            this.emailLabel.TabIndex = 15;
            this.emailLabel.Text = "Email";
            // 
            // orderManagementListBox
            // 
            this.orderManagementListBox.FormattingEnabled = true;
            this.orderManagementListBox.Location = new System.Drawing.Point(8, 171);
            this.orderManagementListBox.Margin = new System.Windows.Forms.Padding(2);
            this.orderManagementListBox.Name = "orderManagementListBox";
            this.orderManagementListBox.ScrollAlwaysVisible = true;
            this.orderManagementListBox.Size = new System.Drawing.Size(519, 108);
            this.orderManagementListBox.TabIndex = 16;
            // 
            // OrderManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(533, 292);
            this.Controls.Add(this.orderManagementListBox);
            this.Controls.Add(this.emailLabel);
            this.Controls.Add(this.amountPendingLabel);
            this.Controls.Add(this.amountPaidLabel);
            this.Controls.Add(this.totalPriceLabel);
            this.Controls.Add(this.bookingIDLabel);
            this.Controls.Add(this.OrderManagementLabel);
            this.Controls.Add(this.updatePaymentButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.backToHomePageButton);
            this.Controls.Add(this.cornerUsernamelabel);
            this.Controls.Add(this.LogoImage);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "OrderManagement";
            this.Text = "Order Management";
            ((System.ComponentModel.ISupportInitialize)(this.LogoImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox LogoImage;
        private System.Windows.Forms.Label cornerUsernamelabel;
        private System.Windows.Forms.Button backToHomePageButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button updatePaymentButton;
        private System.Windows.Forms.Label OrderManagementLabel;
        private System.Windows.Forms.Label bookingIDLabel;
        private System.Windows.Forms.Label totalPriceLabel;
        private System.Windows.Forms.Label amountPaidLabel;
        private System.Windows.Forms.Label amountPendingLabel;
        private System.Windows.Forms.Label emailLabel;
        private System.Windows.Forms.ListBox orderManagementListBox;
    }
}