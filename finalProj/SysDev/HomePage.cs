﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SysDev
{
    public partial class HomePage : Form
    {
        public HomePage()
        {
           // cornerUsernamelabel.Text = SignIn.Username;
            InitializeComponent();
        }

        private void HomePage_Load(object sender, EventArgs e)
        {

        }

        private void placeOrderButton_Click(object sender, EventArgs e)
        {
            seperatorLabel PlaceOrder = new seperatorLabel();
            PlaceOrder.Show();
            this.Close();

        }

        private void orderManagementButton_Click(object sender, EventArgs e)
        {
            OrderManagement ordermanagement = new OrderManagement();
            ordermanagement.Show();
            this.Close();

        }

        private void stockManagementButton_Click(object sender, EventArgs e)
        {
            StockManagement addBook = new StockManagement();
            addBook.Show();
            this.Close();
        }
    }
}
