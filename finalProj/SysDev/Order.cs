//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SysDev
{
    using System;
    using System.Collections.Generic;
    
    public partial class Order
    {
        public int BookingId { get; set; }
        public string Status { get; set; }
        public int UserId { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string Location { get; set; }
        public Nullable<int> RequiredQuantity { get; set; }
        public Nullable<double> TotalPrice { get; set; }
        public int BookId { get; set; }
        public Nullable<double> AmountPaid { get; set; }
        public Nullable<double> AmountPending { get; set; }
    }
}
