﻿namespace SysDev
{
    partial class AddStock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LogoImage = new System.Windows.Forms.PictureBox();
            this.cornerUsernamelabel = new System.Windows.Forms.Label();
            this.backToHomePageButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.uniqueIDLabel = new System.Windows.Forms.Label();
            this.uniqueIDTextBox = new System.Windows.Forms.TextBox();
            this.nameLabel = new System.Windows.Forms.Label();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.authorLabel = new System.Windows.Forms.Label();
            this.authorTextBox = new System.Windows.Forms.TextBox();
            this.isbnLabel = new System.Windows.Forms.Label();
            this.isbnTextBox = new System.Windows.Forms.TextBox();
            this.availableQtyLabel = new System.Windows.Forms.Label();
            this.availableQtyTextBox = new System.Windows.Forms.TextBox();
            this.pricePerQtyLabel = new System.Windows.Forms.Label();
            this.pricePerQtyTextBox = new System.Windows.Forms.TextBox();
            this.locationLabel = new System.Windows.Forms.Label();
            this.locationTextBox = new System.Windows.Forms.TextBox();
            this.addStockButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.LogoImage)).BeginInit();
            this.SuspendLayout();
            // 
            // LogoImage
            // 
            this.LogoImage.Image = global::SysDev.Properties.Resources.unnamed;
            this.LogoImage.Location = new System.Drawing.Point(171, 6);
            this.LogoImage.Margin = new System.Windows.Forms.Padding(2);
            this.LogoImage.Name = "LogoImage";
            this.LogoImage.Size = new System.Drawing.Size(103, 65);
            this.LogoImage.TabIndex = 2;
            this.LogoImage.TabStop = false;
            // 
            // cornerUsernamelabel
            // 
            this.cornerUsernamelabel.AutoSize = true;
            this.cornerUsernamelabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cornerUsernamelabel.Location = new System.Drawing.Point(8, 6);
            this.cornerUsernamelabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.cornerUsernamelabel.Name = "cornerUsernamelabel";
            this.cornerUsernamelabel.Size = new System.Drawing.Size(81, 17);
            this.cornerUsernamelabel.TabIndex = 3;
            this.cornerUsernamelabel.Text = "Username";
            // 
            // backToHomePageButton
            // 
            this.backToHomePageButton.Location = new System.Drawing.Point(11, 36);
            this.backToHomePageButton.Margin = new System.Windows.Forms.Padding(2);
            this.backToHomePageButton.Name = "backToHomePageButton";
            this.backToHomePageButton.Size = new System.Drawing.Size(70, 35);
            this.backToHomePageButton.TabIndex = 6;
            this.backToHomePageButton.Text = "Back to Home page";
            this.backToHomePageButton.UseVisualStyleBackColor = true;
            this.backToHomePageButton.Click += new System.EventHandler(this.backToHomePageButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 103);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 17);
            this.label1.TabIndex = 7;
            this.label1.Text = "New Stock";
            // 
            // uniqueIDLabel
            // 
            this.uniqueIDLabel.AutoSize = true;
            this.uniqueIDLabel.Location = new System.Drawing.Point(11, 142);
            this.uniqueIDLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.uniqueIDLabel.Name = "uniqueIDLabel";
            this.uniqueIDLabel.Size = new System.Drawing.Size(58, 13);
            this.uniqueIDLabel.TabIndex = 8;
            this.uniqueIDLabel.Text = "Unique ID:";
            // 
            // uniqueIDTextBox
            // 
            this.uniqueIDTextBox.Location = new System.Drawing.Point(87, 140);
            this.uniqueIDTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.uniqueIDTextBox.Name = "uniqueIDTextBox";
            this.uniqueIDTextBox.Size = new System.Drawing.Size(100, 20);
            this.uniqueIDTextBox.TabIndex = 9;
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point(11, 171);
            this.nameLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(38, 13);
            this.nameLabel.TabIndex = 10;
            this.nameLabel.Text = "Name:";
            // 
            // nameTextBox
            // 
            this.nameTextBox.Location = new System.Drawing.Point(87, 169);
            this.nameTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(100, 20);
            this.nameTextBox.TabIndex = 11;
            // 
            // authorLabel
            // 
            this.authorLabel.AutoSize = true;
            this.authorLabel.Location = new System.Drawing.Point(11, 197);
            this.authorLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.authorLabel.Name = "authorLabel";
            this.authorLabel.Size = new System.Drawing.Size(41, 13);
            this.authorLabel.TabIndex = 12;
            this.authorLabel.Text = "Author:";
            // 
            // authorTextBox
            // 
            this.authorTextBox.Location = new System.Drawing.Point(87, 195);
            this.authorTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.authorTextBox.Name = "authorTextBox";
            this.authorTextBox.Size = new System.Drawing.Size(100, 20);
            this.authorTextBox.TabIndex = 13;
            // 
            // isbnLabel
            // 
            this.isbnLabel.AutoSize = true;
            this.isbnLabel.Location = new System.Drawing.Point(11, 222);
            this.isbnLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.isbnLabel.Name = "isbnLabel";
            this.isbnLabel.Size = new System.Drawing.Size(35, 13);
            this.isbnLabel.TabIndex = 14;
            this.isbnLabel.Text = "ISBN:";
            // 
            // isbnTextBox
            // 
            this.isbnTextBox.Location = new System.Drawing.Point(87, 222);
            this.isbnTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.isbnTextBox.Name = "isbnTextBox";
            this.isbnTextBox.Size = new System.Drawing.Size(100, 20);
            this.isbnTextBox.TabIndex = 15;
            // 
            // availableQtyLabel
            // 
            this.availableQtyLabel.AutoSize = true;
            this.availableQtyLabel.Location = new System.Drawing.Point(239, 140);
            this.availableQtyLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.availableQtyLabel.Name = "availableQtyLabel";
            this.availableQtyLabel.Size = new System.Drawing.Size(72, 13);
            this.availableQtyLabel.TabIndex = 16;
            this.availableQtyLabel.Text = "Available Qty:";
            // 
            // availableQtyTextBox
            // 
            this.availableQtyTextBox.Location = new System.Drawing.Point(326, 138);
            this.availableQtyTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.availableQtyTextBox.Name = "availableQtyTextBox";
            this.availableQtyTextBox.Size = new System.Drawing.Size(100, 20);
            this.availableQtyTextBox.TabIndex = 17;
            this.availableQtyTextBox.TextChanged += new System.EventHandler(this.availableQtyTextBox_TextChanged);
            // 
            // pricePerQtyLabel
            // 
            this.pricePerQtyLabel.AutoSize = true;
            this.pricePerQtyLabel.Location = new System.Drawing.Point(239, 169);
            this.pricePerQtyLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.pricePerQtyLabel.Name = "pricePerQtyLabel";
            this.pricePerQtyLabel.Size = new System.Drawing.Size(71, 13);
            this.pricePerQtyLabel.TabIndex = 18;
            this.pricePerQtyLabel.Text = "Price per Qty:";
            // 
            // pricePerQtyTextBox
            // 
            this.pricePerQtyTextBox.Location = new System.Drawing.Point(326, 167);
            this.pricePerQtyTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.pricePerQtyTextBox.Name = "pricePerQtyTextBox";
            this.pricePerQtyTextBox.Size = new System.Drawing.Size(100, 20);
            this.pricePerQtyTextBox.TabIndex = 19;
            // 
            // locationLabel
            // 
            this.locationLabel.AutoSize = true;
            this.locationLabel.Location = new System.Drawing.Point(239, 197);
            this.locationLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.locationLabel.Name = "locationLabel";
            this.locationLabel.Size = new System.Drawing.Size(51, 13);
            this.locationLabel.TabIndex = 20;
            this.locationLabel.Text = "Location:";
            // 
            // locationTextBox
            // 
            this.locationTextBox.Location = new System.Drawing.Point(326, 195);
            this.locationTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.locationTextBox.Name = "locationTextBox";
            this.locationTextBox.Size = new System.Drawing.Size(100, 20);
            this.locationTextBox.TabIndex = 21;
            // 
            // addStockButton
            // 
            this.addStockButton.Location = new System.Drawing.Point(193, 272);
            this.addStockButton.Margin = new System.Windows.Forms.Padding(2);
            this.addStockButton.Name = "addStockButton";
            this.addStockButton.Size = new System.Drawing.Size(67, 29);
            this.addStockButton.TabIndex = 22;
            this.addStockButton.Text = "Add Stock";
            this.addStockButton.UseVisualStyleBackColor = true;
            this.addStockButton.Click += new System.EventHandler(this.addStockButton_Click);
            // 
            // AddStock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(456, 326);
            this.Controls.Add(this.addStockButton);
            this.Controls.Add(this.locationTextBox);
            this.Controls.Add(this.locationLabel);
            this.Controls.Add(this.pricePerQtyTextBox);
            this.Controls.Add(this.pricePerQtyLabel);
            this.Controls.Add(this.availableQtyTextBox);
            this.Controls.Add(this.availableQtyLabel);
            this.Controls.Add(this.isbnTextBox);
            this.Controls.Add(this.isbnLabel);
            this.Controls.Add(this.authorTextBox);
            this.Controls.Add(this.authorLabel);
            this.Controls.Add(this.nameTextBox);
            this.Controls.Add(this.nameLabel);
            this.Controls.Add(this.uniqueIDTextBox);
            this.Controls.Add(this.uniqueIDLabel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.backToHomePageButton);
            this.Controls.Add(this.cornerUsernamelabel);
            this.Controls.Add(this.LogoImage);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "AddStock";
            this.Text = "Add Stock";
            ((System.ComponentModel.ISupportInitialize)(this.LogoImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox LogoImage;
        private System.Windows.Forms.Label cornerUsernamelabel;
        private System.Windows.Forms.Button backToHomePageButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label uniqueIDLabel;
        private System.Windows.Forms.TextBox uniqueIDTextBox;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.Label authorLabel;
        private System.Windows.Forms.TextBox authorTextBox;
        private System.Windows.Forms.Label isbnLabel;
        private System.Windows.Forms.TextBox isbnTextBox;
        private System.Windows.Forms.Label availableQtyLabel;
        private System.Windows.Forms.TextBox availableQtyTextBox;
        private System.Windows.Forms.Label pricePerQtyLabel;
        private System.Windows.Forms.TextBox pricePerQtyTextBox;
        private System.Windows.Forms.Label locationLabel;
        private System.Windows.Forms.TextBox locationTextBox;
        private System.Windows.Forms.Button addStockButton;
    }
}