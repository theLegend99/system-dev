﻿using System;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Net.Mail;

namespace SysDev
{
    public partial class Creation : Form
    {
        private SysDev.AccountListEntities2 dbAccountContext =
            new SysDev.AccountListEntities2();

        private SysDev.InventoryEntities dbInventoryContext =
            new SysDev.InventoryEntities();

        private SysDev.OrderEntities dbOrderContext =
            new SysDev.OrderEntities();

        public Creation()
        {
            InitializeComponent();
        }

        private void creationSubmitButton_Click(object sender, EventArgs e)
        {
            if (VerifyEmail(creationUsernameTextBox.Text))
            {
                String query = "INSERT INTO AccountList(UserId, FirstName, LastName, Role, Email, PassWord)";
                query += " VALUES (@UserId, @FirstName, @LastName, @Role, @Email, @PassWord)";
                SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|AccountList.mdf;Integrated Security=True;Connect Timeout=30");
                SqlCommand cmd = new SqlCommand(query, con);

                cmd.Parameters.AddWithValue("@UserId", userIdTextBox.Text);
                cmd.Parameters.AddWithValue("@FirstName", firstNameTextBox.Text);
                cmd.Parameters.AddWithValue("@LastName", lastNameTextBox.Text);
                cmd.Parameters.AddWithValue("@Role", roleTextBox.Text);
                cmd.Parameters.AddWithValue("@Email", creationUsernameTextBox.Text);
                cmd.Parameters.AddWithValue("@PassWord", creationPasswordTextBox.Text);
                con.Open();
                int i = cmd.ExecuteNonQuery();

                con.Close();

                if (i != 0)
                {
                    MessageBox.Show(i + "Data Saved");
                }
                else
                    MessageBox.Show(i + "error");

                this.Close();
            }
            else
            {
                MessageBox.Show("Invalid Email");
            }

        }

        private Boolean VerifyEmail(String email)
        {
            try
            {
                MailAddress m = new MailAddress(email);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
