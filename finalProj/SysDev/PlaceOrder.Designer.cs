﻿namespace SysDev
{
    partial class seperatorLabel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LogoImage = new System.Windows.Forms.PictureBox();
            this.cornerUsernamelabel = new System.Windows.Forms.Label();
            this.backToHomePageButton = new System.Windows.Forms.Button();
            this.placeOrderLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.searchBookslabel = new System.Windows.Forms.Label();
            this.searchLocationLabel = new System.Windows.Forms.Label();
            this.searchLocationComboBox = new System.Windows.Forms.ComboBox();
            this.bookNameLabel = new System.Windows.Forms.Label();
            this.bookNameTextBox = new System.Windows.Forms.TextBox();
            this.searchButton = new System.Windows.Forms.Button();
            this.searchResultslabel = new System.Windows.Forms.Label();
            this.placeOrderListBox = new System.Windows.Forms.ListBox();
            this.priceLabel = new System.Windows.Forms.Label();
            this.availableQtyLabel = new System.Windows.Forms.Label();
            this.requiredQtyLabel = new System.Windows.Forms.Label();
            this.seperatorLabel2 = new System.Windows.Forms.Label();
            this.addButton = new System.Windows.Forms.Button();
            this.orderSummaryLabel = new System.Windows.Forms.Label();
            this.qtyLabel = new System.Windows.Forms.Label();
            this.totalPriceLabel = new System.Windows.Forms.Label();
            this.orderSummaryListBox = new System.Windows.Forms.ListBox();
            this.placeOrderButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.LogoImage)).BeginInit();
            this.SuspendLayout();
            // 
            // LogoImage
            // 
            this.LogoImage.Image = global::SysDev.Properties.Resources.unnamed;
            this.LogoImage.Location = new System.Drawing.Point(203, 13);
            this.LogoImage.Margin = new System.Windows.Forms.Padding(2);
            this.LogoImage.Name = "LogoImage";
            this.LogoImage.Size = new System.Drawing.Size(103, 65);
            this.LogoImage.TabIndex = 2;
            this.LogoImage.TabStop = false;
            // 
            // cornerUsernamelabel
            // 
            this.cornerUsernamelabel.AutoSize = true;
            this.cornerUsernamelabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cornerUsernamelabel.Location = new System.Drawing.Point(8, 13);
            this.cornerUsernamelabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.cornerUsernamelabel.Name = "cornerUsernamelabel";
            this.cornerUsernamelabel.Size = new System.Drawing.Size(81, 17);
            this.cornerUsernamelabel.TabIndex = 3;
            this.cornerUsernamelabel.Text = "Username";
            // 
            // backToHomePageButton
            // 
            this.backToHomePageButton.Location = new System.Drawing.Point(11, 53);
            this.backToHomePageButton.Margin = new System.Windows.Forms.Padding(2);
            this.backToHomePageButton.Name = "backToHomePageButton";
            this.backToHomePageButton.Size = new System.Drawing.Size(70, 35);
            this.backToHomePageButton.TabIndex = 4;
            this.backToHomePageButton.Text = "Back to Home page";
            this.backToHomePageButton.UseVisualStyleBackColor = true;
            this.backToHomePageButton.Click += new System.EventHandler(this.backToHomePageButton_Click);
            // 
            // placeOrderLabel
            // 
            this.placeOrderLabel.AutoSize = true;
            this.placeOrderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.placeOrderLabel.Location = new System.Drawing.Point(8, 116);
            this.placeOrderLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.placeOrderLabel.Name = "placeOrderLabel";
            this.placeOrderLabel.Size = new System.Drawing.Size(95, 17);
            this.placeOrderLabel.TabIndex = 5;
            this.placeOrderLabel.Text = "Place Order";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(-9, 132);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(962, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "_________________________________________________________________________________" +
    "_________________________";
            // 
            // searchBookslabel
            // 
            this.searchBookslabel.AutoSize = true;
            this.searchBookslabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchBookslabel.Location = new System.Drawing.Point(8, 158);
            this.searchBookslabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.searchBookslabel.Name = "searchBookslabel";
            this.searchBookslabel.Size = new System.Drawing.Size(96, 17);
            this.searchBookslabel.TabIndex = 7;
            this.searchBookslabel.Text = "Search Books";
            // 
            // searchLocationLabel
            // 
            this.searchLocationLabel.AutoSize = true;
            this.searchLocationLabel.Location = new System.Drawing.Point(11, 194);
            this.searchLocationLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.searchLocationLabel.Name = "searchLocationLabel";
            this.searchLocationLabel.Size = new System.Drawing.Size(88, 13);
            this.searchLocationLabel.TabIndex = 8;
            this.searchLocationLabel.Text = "Search Location:";
            // 
            // searchLocationComboBox
            // 
            this.searchLocationComboBox.AllowDrop = true;
            this.searchLocationComboBox.FormattingEnabled = true;
            this.searchLocationComboBox.Items.AddRange(new object[] {
            "Chicago",
            "London",
            "Toronto"});
            this.searchLocationComboBox.Location = new System.Drawing.Point(101, 192);
            this.searchLocationComboBox.Margin = new System.Windows.Forms.Padding(2);
            this.searchLocationComboBox.Name = "searchLocationComboBox";
            this.searchLocationComboBox.Size = new System.Drawing.Size(117, 21);
            this.searchLocationComboBox.TabIndex = 9;
            // 
            // bookNameLabel
            // 
            this.bookNameLabel.AutoSize = true;
            this.bookNameLabel.Location = new System.Drawing.Point(245, 196);
            this.bookNameLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.bookNameLabel.Name = "bookNameLabel";
            this.bookNameLabel.Size = new System.Drawing.Size(66, 13);
            this.bookNameLabel.TabIndex = 10;
            this.bookNameLabel.Text = "Book Name:";
            // 
            // bookNameTextBox
            // 
            this.bookNameTextBox.Location = new System.Drawing.Point(313, 192);
            this.bookNameTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.bookNameTextBox.Name = "bookNameTextBox";
            this.bookNameTextBox.Size = new System.Drawing.Size(117, 20);
            this.bookNameTextBox.TabIndex = 11;
            // 
            // searchButton
            // 
            this.searchButton.Location = new System.Drawing.Point(454, 190);
            this.searchButton.Margin = new System.Windows.Forms.Padding(2);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(56, 23);
            this.searchButton.TabIndex = 12;
            this.searchButton.Text = "Search";
            this.searchButton.UseVisualStyleBackColor = true;
            this.searchButton.Click += new System.EventHandler(this.searchButton_Click);
            // 
            // searchResultslabel
            // 
            this.searchResultslabel.AutoSize = true;
            this.searchResultslabel.Location = new System.Drawing.Point(71, 240);
            this.searchResultslabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.searchResultslabel.Name = "searchResultslabel";
            this.searchResultslabel.Size = new System.Drawing.Size(66, 13);
            this.searchResultslabel.TabIndex = 13;
            this.searchResultslabel.Text = "Book Name:";
            // 
            // placeOrderListBox
            // 
            this.placeOrderListBox.FormattingEnabled = true;
            this.placeOrderListBox.Location = new System.Drawing.Point(57, 264);
            this.placeOrderListBox.Margin = new System.Windows.Forms.Padding(2);
            this.placeOrderListBox.Name = "placeOrderListBox";
            this.placeOrderListBox.ScrollAlwaysVisible = true;
            this.placeOrderListBox.Size = new System.Drawing.Size(419, 82);
            this.placeOrderListBox.TabIndex = 14;
            // 
            // priceLabel
            // 
            this.priceLabel.AutoSize = true;
            this.priceLabel.Location = new System.Drawing.Point(206, 240);
            this.priceLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.priceLabel.Name = "priceLabel";
            this.priceLabel.Size = new System.Drawing.Size(31, 13);
            this.priceLabel.TabIndex = 15;
            this.priceLabel.Text = "Price";
            // 
            // availableQtyLabel
            // 
            this.availableQtyLabel.AutoSize = true;
            this.availableQtyLabel.Location = new System.Drawing.Point(272, 240);
            this.availableQtyLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.availableQtyLabel.Name = "availableQtyLabel";
            this.availableQtyLabel.Size = new System.Drawing.Size(69, 13);
            this.availableQtyLabel.TabIndex = 16;
            this.availableQtyLabel.Text = "Available Qty";
            // 
            // requiredQtyLabel
            // 
            this.requiredQtyLabel.AutoSize = true;
            this.requiredQtyLabel.Location = new System.Drawing.Point(369, 240);
            this.requiredQtyLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.requiredQtyLabel.Name = "requiredQtyLabel";
            this.requiredQtyLabel.Size = new System.Drawing.Size(69, 13);
            this.requiredQtyLabel.TabIndex = 17;
            this.requiredQtyLabel.Text = "Required Qty";
            // 
            // seperatorLabel2
            // 
            this.seperatorLabel2.AutoSize = true;
            this.seperatorLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.seperatorLabel2.Location = new System.Drawing.Point(-165, 378);
            this.seperatorLabel2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.seperatorLabel2.Name = "seperatorLabel2";
            this.seperatorLabel2.Size = new System.Drawing.Size(962, 17);
            this.seperatorLabel2.TabIndex = 18;
            this.seperatorLabel2.Text = "_________________________________________________________________________________" +
    "_________________________";
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(227, 355);
            this.addButton.Margin = new System.Windows.Forms.Padding(2);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(66, 27);
            this.addButton.TabIndex = 19;
            this.addButton.Text = "Add";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // orderSummaryLabel
            // 
            this.orderSummaryLabel.AutoSize = true;
            this.orderSummaryLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.orderSummaryLabel.Location = new System.Drawing.Point(78, 401);
            this.orderSummaryLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.orderSummaryLabel.Name = "orderSummaryLabel";
            this.orderSummaryLabel.Size = new System.Drawing.Size(85, 17);
            this.orderSummaryLabel.TabIndex = 20;
            this.orderSummaryLabel.Text = "Book Name:";
            // 
            // qtyLabel
            // 
            this.qtyLabel.AutoSize = true;
            this.qtyLabel.Location = new System.Drawing.Point(253, 404);
            this.qtyLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.qtyLabel.Name = "qtyLabel";
            this.qtyLabel.Size = new System.Drawing.Size(23, 13);
            this.qtyLabel.TabIndex = 21;
            this.qtyLabel.Text = "Qty";
            // 
            // totalPriceLabel
            // 
            this.totalPriceLabel.AutoSize = true;
            this.totalPriceLabel.Location = new System.Drawing.Point(341, 404);
            this.totalPriceLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.totalPriceLabel.Name = "totalPriceLabel";
            this.totalPriceLabel.Size = new System.Drawing.Size(58, 13);
            this.totalPriceLabel.TabIndex = 22;
            this.totalPriceLabel.Text = "Total Price";
            // 
            // orderSummaryListBox
            // 
            this.orderSummaryListBox.FormattingEnabled = true;
            this.orderSummaryListBox.Location = new System.Drawing.Point(101, 428);
            this.orderSummaryListBox.Margin = new System.Windows.Forms.Padding(2);
            this.orderSummaryListBox.Name = "orderSummaryListBox";
            this.orderSummaryListBox.ScrollAlwaysVisible = true;
            this.orderSummaryListBox.Size = new System.Drawing.Size(337, 82);
            this.orderSummaryListBox.TabIndex = 23;
            this.orderSummaryListBox.SelectedIndexChanged += new System.EventHandler(this.orderSummaryListBox_SelectedIndexChanged);
            // 
            // placeOrderButton
            // 
            this.placeOrderButton.Location = new System.Drawing.Point(227, 518);
            this.placeOrderButton.Margin = new System.Windows.Forms.Padding(2);
            this.placeOrderButton.Name = "placeOrderButton";
            this.placeOrderButton.Size = new System.Drawing.Size(79, 27);
            this.placeOrderButton.TabIndex = 24;
            this.placeOrderButton.Text = "Place Order";
            this.placeOrderButton.UseVisualStyleBackColor = true;
            this.placeOrderButton.Click += new System.EventHandler(this.placeOrderButton_Click);
            // 
            // seperatorLabel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(533, 564);
            this.Controls.Add(this.placeOrderButton);
            this.Controls.Add(this.orderSummaryListBox);
            this.Controls.Add(this.totalPriceLabel);
            this.Controls.Add(this.qtyLabel);
            this.Controls.Add(this.orderSummaryLabel);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.seperatorLabel2);
            this.Controls.Add(this.requiredQtyLabel);
            this.Controls.Add(this.availableQtyLabel);
            this.Controls.Add(this.priceLabel);
            this.Controls.Add(this.placeOrderListBox);
            this.Controls.Add(this.searchResultslabel);
            this.Controls.Add(this.searchButton);
            this.Controls.Add(this.bookNameTextBox);
            this.Controls.Add(this.bookNameLabel);
            this.Controls.Add(this.searchLocationComboBox);
            this.Controls.Add(this.searchLocationLabel);
            this.Controls.Add(this.searchBookslabel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.placeOrderLabel);
            this.Controls.Add(this.backToHomePageButton);
            this.Controls.Add(this.cornerUsernamelabel);
            this.Controls.Add(this.LogoImage);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "seperatorLabel";
            this.Text = "Place Order";
            ((System.ComponentModel.ISupportInitialize)(this.LogoImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox LogoImage;
        private System.Windows.Forms.Label cornerUsernamelabel;
        private System.Windows.Forms.Button backToHomePageButton;
        private System.Windows.Forms.Label placeOrderLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label searchBookslabel;
        private System.Windows.Forms.Label searchLocationLabel;
        private System.Windows.Forms.ComboBox searchLocationComboBox;
        private System.Windows.Forms.Label bookNameLabel;
        private System.Windows.Forms.TextBox bookNameTextBox;
        private System.Windows.Forms.Button searchButton;
        private System.Windows.Forms.Label searchResultslabel;
        private System.Windows.Forms.ListBox placeOrderListBox;
        private System.Windows.Forms.Label priceLabel;
        private System.Windows.Forms.Label availableQtyLabel;
        private System.Windows.Forms.Label requiredQtyLabel;
        private System.Windows.Forms.Label seperatorLabel2;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Label orderSummaryLabel;
        private System.Windows.Forms.Label qtyLabel;
        private System.Windows.Forms.Label totalPriceLabel;
        private System.Windows.Forms.ListBox orderSummaryListBox;
        private System.Windows.Forms.Button placeOrderButton;
    }
}