﻿namespace SysDev
{
    partial class HomePage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LogoImage = new System.Windows.Forms.PictureBox();
            this.cornerUsernamelabel = new System.Windows.Forms.Label();
            this.placeOrderButton = new System.Windows.Forms.Button();
            this.orderManagementButton = new System.Windows.Forms.Button();
            this.stockManagementButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.LogoImage)).BeginInit();
            this.SuspendLayout();
            // 
            // LogoImage
            // 
            this.LogoImage.Image = global::SysDev.Properties.Resources.unnamed;
            this.LogoImage.Location = new System.Drawing.Point(172, 16);
            this.LogoImage.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.LogoImage.Name = "LogoImage";
            this.LogoImage.Size = new System.Drawing.Size(103, 65);
            this.LogoImage.TabIndex = 1;
            this.LogoImage.TabStop = false;
            // 
            // cornerUsernamelabel
            // 
            this.cornerUsernamelabel.AutoSize = true;
            this.cornerUsernamelabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cornerUsernamelabel.Location = new System.Drawing.Point(9, 16);
            this.cornerUsernamelabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.cornerUsernamelabel.Name = "cornerUsernamelabel";
            this.cornerUsernamelabel.Size = new System.Drawing.Size(81, 17);
            this.cornerUsernamelabel.TabIndex = 2;
            this.cornerUsernamelabel.Text = "Username";
            // 
            // placeOrderButton
            // 
            this.placeOrderButton.Location = new System.Drawing.Point(63, 135);
            this.placeOrderButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.placeOrderButton.Name = "placeOrderButton";
            this.placeOrderButton.Size = new System.Drawing.Size(65, 44);
            this.placeOrderButton.TabIndex = 3;
            this.placeOrderButton.Text = "Place Order";
            this.placeOrderButton.UseVisualStyleBackColor = true;
            this.placeOrderButton.Click += new System.EventHandler(this.placeOrderButton_Click);
            // 
            // orderManagementButton
            // 
            this.orderManagementButton.Location = new System.Drawing.Point(172, 134);
            this.orderManagementButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.orderManagementButton.Name = "orderManagementButton";
            this.orderManagementButton.Size = new System.Drawing.Size(79, 45);
            this.orderManagementButton.TabIndex = 4;
            this.orderManagementButton.Text = "Order Management";
            this.orderManagementButton.UseVisualStyleBackColor = true;
            this.orderManagementButton.Click += new System.EventHandler(this.orderManagementButton_Click);
            // 
            // stockManagementButton
            // 
            this.stockManagementButton.Location = new System.Drawing.Point(289, 135);
            this.stockManagementButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.stockManagementButton.Name = "stockManagementButton";
            this.stockManagementButton.Size = new System.Drawing.Size(81, 45);
            this.stockManagementButton.TabIndex = 5;
            this.stockManagementButton.Text = "Stock Management";
            this.stockManagementButton.UseVisualStyleBackColor = true;
            this.stockManagementButton.Click += new System.EventHandler(this.stockManagementButton_Click);
            // 
            // HomePage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(466, 238);
            this.Controls.Add(this.stockManagementButton);
            this.Controls.Add(this.orderManagementButton);
            this.Controls.Add(this.placeOrderButton);
            this.Controls.Add(this.cornerUsernamelabel);
            this.Controls.Add(this.LogoImage);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "HomePage";
            this.Text = "Home Page";
            this.Load += new System.EventHandler(this.HomePage_Load);
            ((System.ComponentModel.ISupportInitialize)(this.LogoImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox LogoImage;
        private System.Windows.Forms.Label cornerUsernamelabel;
        private System.Windows.Forms.Button placeOrderButton;
        private System.Windows.Forms.Button orderManagementButton;
        private System.Windows.Forms.Button stockManagementButton;
    }
}