﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SysDev
{
    public partial class seperatorLabel : Form
    {
        private String Book;
        private int quantity;
            private String location;
            private decimal price;
        private int offset;
        private int j = 0;

        public seperatorLabel()
        {
            InitializeComponent();
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            string query = "Select * from Inventory WHERE Location = @Location AND BookTitle = @BookTitle AND BookTitle = @BookTitle ";
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|Inventory.mdf;Integrated Security=True;Connect Timeout=30");
            SqlCommand cmd = new SqlCommand(query, con);
            using (con)
            {
                cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@Location", searchLocationComboBox.GetItemText(searchLocationComboBox.SelectedItem));
                cmd.Parameters.AddWithValue("@BookTitle", bookNameTextBox.Text);
                //cmd.Parameters.AddWithValue("@Price", price);
                //cmd.Parameters.AddWithValue("@AvailableQuantity", bookNameTextBox.Text);

                con.Open();
                using (SqlDataReader oReader = cmd.ExecuteReader())
                {
                    while (oReader.Read())
                    {
                        
                        if (searchLocationComboBox.GetItemText(searchLocationComboBox.SelectedItem).Equals(oReader["Location"].ToString()) &&  bookNameTextBox.Text.Equals(oReader["BookTitle"].ToString()))
                        {
                            Book = oReader["BookTitle"].ToString();
                            quantity = int.Parse(oReader["AvailableQuantity"].ToString());
                            location = oReader["Location"].ToString();
                            price = decimal.Parse(oReader["Price"].ToString());
                           

                            MessageBox.Show("retirved atat successfullly" + Book + " " +  quantity + " " + location + " " + price);
                            placeOrderListBox.Items.Add(String.Format("\n{0,-47}{1,-25}{2,-30}{3,-25}\n\n", oReader["BookTitle"], oReader["Price"], oReader["AvailableQuantity"], 0));
                        }
                        
                    }
                    con.Close();
                }
            }
        }

        private void backToHomePageButton_Click(object sender, EventArgs e)
        {
            HomePage homePage = new HomePage();
            homePage.Show();
            this.Close();
        }

        private void placeOrderButton_Click(object sender, EventArgs e)
        {
            string query = "Select * from Orders WHERE BookName = @BookName";
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|Orders.mdf;Integrated Security=True;Connect Timeout=30");
            SqlCommand cmd = new SqlCommand(query, con);
            using (con)
            {
                cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@BookName", Book);
                con.Open();
                using (SqlDataReader oReader = cmd.ExecuteReader())
                {
                    while (oReader.Read())
                    {

                        orderSummaryListBox.Items.Add(String.Format("\n{0,-30}{1,-25}{2,-25}\n\n", oReader["BookName"], quantity, quantity * (int)price));
                    }
                    con.Close();
                }
            }
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            j++;
            DateTime thisDay = DateTime.Today;

            String query = "INSERT INTO Orders(BookName, BookingId, Status, UserId, CreatedDate, Location, RequiredQuantity, TotalPrice, BookId, AmountPaid, AmountPending)";
            query += " VALUES (@BookName, @BookingId, @Status, @UserId, @CreatedDate, @Location, @RequiredQuantity, @TotalPrice, @BookId, @AmountPaid, @AmountPending)";

            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|Orders.mdf;Integrated Security=True;Connect Timeout=30");

            SqlCommand cmd = new SqlCommand(query, con);


            cmd.Parameters.AddWithValue("@BookName", Book);
            cmd.Parameters.AddWithValue("@BookingId", AddStock.BookingId);
            cmd.Parameters.AddWithValue("@Status", "pending");
            cmd.Parameters.AddWithValue("@UserId", "12");
            cmd.Parameters.AddWithValue("@CreatedDate", thisDay.ToString());
            cmd.Parameters.AddWithValue("@Location", location);
            cmd.Parameters.AddWithValue("@RequiredQuantity", quantity);
            cmd.Parameters.AddWithValue("@TotalPrice", price * quantity);
            cmd.Parameters.AddWithValue("@BookId", "1");
            cmd.Parameters.AddWithValue("@AmountPaid", "0");
            cmd.Parameters.AddWithValue("@AmountPending", "0");
        

            con.Open();
            int i = cmd.ExecuteNonQuery();

            con.Close();

            if (i != 0)
            {
                MessageBox.Show(i + "Data Saved");
            }
            else
                MessageBox.Show(i + "error");


            offset = j;

        }

        private void orderSummaryListBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
