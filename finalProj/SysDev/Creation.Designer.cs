﻿namespace SysDev
{
    partial class Creation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LogoImage = new System.Windows.Forms.PictureBox();
            this.titleLabel = new System.Windows.Forms.Label();
            this.titleComboBox = new System.Windows.Forms.ComboBox();
            this.firstNameTextBox = new System.Windows.Forms.TextBox();
            this.firstNameLabel = new System.Windows.Forms.Label();
            this.lastNamelabel = new System.Windows.Forms.Label();
            this.lastNameTextBox = new System.Windows.Forms.TextBox();
            this.roleLabel = new System.Windows.Forms.Label();
            this.roleTextBox = new System.Windows.Forms.TextBox();
            this.creationUsernameLabel = new System.Windows.Forms.Label();
            this.creationUsernameTextBox = new System.Windows.Forms.TextBox();
            this.creationPasswordLabel = new System.Windows.Forms.Label();
            this.creationPasswordTextBox = new System.Windows.Forms.TextBox();
            this.creationSubmitButton = new System.Windows.Forms.Button();
            this.userIdTextBox = new System.Windows.Forms.TextBox();
            this.userIdLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.LogoImage)).BeginInit();
            this.SuspendLayout();
            // 
            // LogoImage
            // 
            this.LogoImage.Image = global::SysDev.Properties.Resources.unnamed;
            this.LogoImage.Location = new System.Drawing.Point(175, 17);
            this.LogoImage.Margin = new System.Windows.Forms.Padding(2);
            this.LogoImage.Name = "LogoImage";
            this.LogoImage.Size = new System.Drawing.Size(107, 68);
            this.LogoImage.TabIndex = 1;
            this.LogoImage.TabStop = false;
            // 
            // titleLabel
            // 
            this.titleLabel.AutoSize = true;
            this.titleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titleLabel.Location = new System.Drawing.Point(147, 107);
            this.titleLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(55, 18);
            this.titleLabel.TabIndex = 2;
            this.titleLabel.Text = "Title  :";
            // 
            // titleComboBox
            // 
            this.titleComboBox.FormattingEnabled = true;
            this.titleComboBox.Location = new System.Drawing.Point(217, 106);
            this.titleComboBox.Margin = new System.Windows.Forms.Padding(2);
            this.titleComboBox.Name = "titleComboBox";
            this.titleComboBox.Size = new System.Drawing.Size(122, 21);
            this.titleComboBox.TabIndex = 3;
            // 
            // firstNameTextBox
            // 
            this.firstNameTextBox.Location = new System.Drawing.Point(217, 179);
            this.firstNameTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.firstNameTextBox.Name = "firstNameTextBox";
            this.firstNameTextBox.Size = new System.Drawing.Size(122, 20);
            this.firstNameTextBox.TabIndex = 4;
            // 
            // firstNameLabel
            // 
            this.firstNameLabel.AutoSize = true;
            this.firstNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firstNameLabel.Location = new System.Drawing.Point(103, 179);
            this.firstNameLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.firstNameLabel.Name = "firstNameLabel";
            this.firstNameLabel.Size = new System.Drawing.Size(106, 18);
            this.firstNameLabel.TabIndex = 5;
            this.firstNameLabel.Text = "First Name  :";
            // 
            // lastNamelabel
            // 
            this.lastNamelabel.AutoSize = true;
            this.lastNamelabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lastNamelabel.Location = new System.Drawing.Point(103, 218);
            this.lastNamelabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lastNamelabel.Name = "lastNamelabel";
            this.lastNamelabel.Size = new System.Drawing.Size(104, 18);
            this.lastNamelabel.TabIndex = 6;
            this.lastNamelabel.Text = "Last Name  :";
            // 
            // lastNameTextBox
            // 
            this.lastNameTextBox.Location = new System.Drawing.Point(217, 220);
            this.lastNameTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.lastNameTextBox.Name = "lastNameTextBox";
            this.lastNameTextBox.Size = new System.Drawing.Size(122, 20);
            this.lastNameTextBox.TabIndex = 7;
            // 
            // roleLabel
            // 
            this.roleLabel.AutoSize = true;
            this.roleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.roleLabel.Location = new System.Drawing.Point(147, 256);
            this.roleLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.roleLabel.Name = "roleLabel";
            this.roleLabel.Size = new System.Drawing.Size(58, 18);
            this.roleLabel.TabIndex = 8;
            this.roleLabel.Text = "Role  :";
            // 
            // roleTextBox
            // 
            this.roleTextBox.Location = new System.Drawing.Point(217, 256);
            this.roleTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.roleTextBox.Name = "roleTextBox";
            this.roleTextBox.Size = new System.Drawing.Size(122, 20);
            this.roleTextBox.TabIndex = 9;
            // 
            // creationUsernameLabel
            // 
            this.creationUsernameLabel.AutoSize = true;
            this.creationUsernameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.creationUsernameLabel.Location = new System.Drawing.Point(54, 289);
            this.creationUsernameLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.creationUsernameLabel.Name = "creationUsernameLabel";
            this.creationUsernameLabel.Size = new System.Drawing.Size(157, 18);
            this.creationUsernameLabel.TabIndex = 10;
            this.creationUsernameLabel.Text = "Username (email)  :";
            // 
            // creationUsernameTextBox
            // 
            this.creationUsernameTextBox.Location = new System.Drawing.Point(217, 291);
            this.creationUsernameTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.creationUsernameTextBox.Name = "creationUsernameTextBox";
            this.creationUsernameTextBox.Size = new System.Drawing.Size(122, 20);
            this.creationUsernameTextBox.TabIndex = 11;
            // 
            // creationPasswordLabel
            // 
            this.creationPasswordLabel.AutoSize = true;
            this.creationPasswordLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.creationPasswordLabel.Location = new System.Drawing.Point(107, 326);
            this.creationPasswordLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.creationPasswordLabel.Name = "creationPasswordLabel";
            this.creationPasswordLabel.Size = new System.Drawing.Size(98, 18);
            this.creationPasswordLabel.TabIndex = 12;
            this.creationPasswordLabel.Text = "Password  :";
            // 
            // creationPasswordTextBox
            // 
            this.creationPasswordTextBox.Location = new System.Drawing.Point(217, 326);
            this.creationPasswordTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.creationPasswordTextBox.Name = "creationPasswordTextBox";
            this.creationPasswordTextBox.Size = new System.Drawing.Size(122, 20);
            this.creationPasswordTextBox.TabIndex = 13;
            // 
            // creationSubmitButton
            // 
            this.creationSubmitButton.Location = new System.Drawing.Point(187, 357);
            this.creationSubmitButton.Margin = new System.Windows.Forms.Padding(2);
            this.creationSubmitButton.Name = "creationSubmitButton";
            this.creationSubmitButton.Size = new System.Drawing.Size(68, 30);
            this.creationSubmitButton.TabIndex = 14;
            this.creationSubmitButton.Text = "Submit";
            this.creationSubmitButton.UseVisualStyleBackColor = true;
            this.creationSubmitButton.Click += new System.EventHandler(this.creationSubmitButton_Click);
            // 
            // userIdTextBox
            // 
            this.userIdTextBox.Location = new System.Drawing.Point(217, 144);
            this.userIdTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.userIdTextBox.Name = "userIdTextBox";
            this.userIdTextBox.Size = new System.Drawing.Size(122, 20);
            this.userIdTextBox.TabIndex = 15;
            // 
            // userIdLabel
            // 
            this.userIdLabel.AutoSize = true;
            this.userIdLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.userIdLabel.Location = new System.Drawing.Point(134, 143);
            this.userIdLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.userIdLabel.Name = "userIdLabel";
            this.userIdLabel.Size = new System.Drawing.Size(72, 18);
            this.userIdLabel.TabIndex = 16;
            this.userIdLabel.Text = "UserId  :";
            this.userIdLabel.Click += new System.EventHandler(this.label1_Click);
            // 
            // Creation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(458, 414);
            this.Controls.Add(this.userIdLabel);
            this.Controls.Add(this.userIdTextBox);
            this.Controls.Add(this.creationSubmitButton);
            this.Controls.Add(this.creationPasswordTextBox);
            this.Controls.Add(this.creationPasswordLabel);
            this.Controls.Add(this.creationUsernameTextBox);
            this.Controls.Add(this.creationUsernameLabel);
            this.Controls.Add(this.roleTextBox);
            this.Controls.Add(this.roleLabel);
            this.Controls.Add(this.lastNameTextBox);
            this.Controls.Add(this.lastNamelabel);
            this.Controls.Add(this.firstNameLabel);
            this.Controls.Add(this.firstNameTextBox);
            this.Controls.Add(this.titleComboBox);
            this.Controls.Add(this.titleLabel);
            this.Controls.Add(this.LogoImage);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Creation";
            this.Text = "Create Account";
            ((System.ComponentModel.ISupportInitialize)(this.LogoImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox LogoImage;
        private System.Windows.Forms.Label titleLabel;
        private System.Windows.Forms.ComboBox titleComboBox;
        private System.Windows.Forms.TextBox firstNameTextBox;
        private System.Windows.Forms.Label firstNameLabel;
        private System.Windows.Forms.Label lastNamelabel;
        private System.Windows.Forms.TextBox lastNameTextBox;
        private System.Windows.Forms.Label roleLabel;
        private System.Windows.Forms.TextBox roleTextBox;
        private System.Windows.Forms.Label creationUsernameLabel;
        private System.Windows.Forms.TextBox creationUsernameTextBox;
        private System.Windows.Forms.Label creationPasswordLabel;
        private System.Windows.Forms.TextBox creationPasswordTextBox;
        private System.Windows.Forms.Button creationSubmitButton;
        private System.Windows.Forms.TextBox userIdTextBox;
        private System.Windows.Forms.Label userIdLabel;
    }
}