﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SysDev
{
    public partial class AddStock : Form
    {
        public static string BookingId;
        public AddStock()
        {
            InitializeComponent();
        }

        private void backToHomePageButton_Click(object sender, EventArgs e)
        {
            HomePage homePage = new HomePage();
            homePage.Show();
            this.Close();
        }

        private void addStockButton_Click(object sender, EventArgs e)
        {
        
            String query = "INSERT INTO Inventory(BookId, BookTitle, Author, ISBN, Price, AvailableQuantity, Location)";
            query += " VALUES (@BookId, @BookTitle, @Author, @ISBN, @Price, @AvailableQuantity, @Location)";
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|Inventory.mdf;Integrated Security=True;Connect Timeout=30");
            SqlCommand cmd = new SqlCommand(query, con);

            cmd.Parameters.AddWithValue("@BookId", uniqueIDTextBox.Text);
            cmd.Parameters.AddWithValue("@BookTitle", nameTextBox.Text);
            cmd.Parameters.AddWithValue("@Author", authorTextBox.Text);
            cmd.Parameters.AddWithValue("@ISBN", isbnTextBox.Text);
            cmd.Parameters.AddWithValue("@Price", pricePerQtyTextBox.Text);
            cmd.Parameters.AddWithValue("@AvailableQuantity", availableQtyTextBox.Text);
            cmd.Parameters.AddWithValue("@Location", locationTextBox.Text);
            con.Open();
            int i = cmd.ExecuteNonQuery();

            con.Close();

            if (i != 0)
            {
                BookingId = uniqueIDTextBox.Text;
                MessageBox.Show(i + "Data Saved");
            }
            else
                MessageBox.Show(i + "error");

            HomePage homePage = new HomePage();
            homePage.Show();
            this.Close();
        }

        private void availableQtyTextBox_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
