﻿namespace SysDev
{
    partial class StockManagement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LogoImage = new System.Windows.Forms.PictureBox();
            this.cornerUsernamelabel = new System.Windows.Forms.Label();
            this.backToHomePageButton = new System.Windows.Forms.Button();
            this.stockManagementLabel = new System.Windows.Forms.Label();
            this.addStockButton = new System.Windows.Forms.Button();
            this.editButton = new System.Windows.Forms.Button();
            this.availableStockLabel = new System.Windows.Forms.Label();
            this.bookNameLabel = new System.Windows.Forms.Label();
            this.availabilityLabel = new System.Windows.Forms.Label();
            this.priceLabel = new System.Windows.Forms.Label();
            this.locationLabel = new System.Windows.Forms.Label();
            this.stockManagementListBox = new System.Windows.Forms.ListBox();
            this.updateButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.LogoImage)).BeginInit();
            this.SuspendLayout();
            // 
            // LogoImage
            // 
            this.LogoImage.Image = global::SysDev.Properties.Resources.unnamed;
            this.LogoImage.Location = new System.Drawing.Point(209, 8);
            this.LogoImage.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.LogoImage.Name = "LogoImage";
            this.LogoImage.Size = new System.Drawing.Size(103, 65);
            this.LogoImage.TabIndex = 3;
            this.LogoImage.TabStop = false;
            // 
            // cornerUsernamelabel
            // 
            this.cornerUsernamelabel.AutoSize = true;
            this.cornerUsernamelabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cornerUsernamelabel.Location = new System.Drawing.Point(8, 6);
            this.cornerUsernamelabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.cornerUsernamelabel.Name = "cornerUsernamelabel";
            this.cornerUsernamelabel.Size = new System.Drawing.Size(81, 17);
            this.cornerUsernamelabel.TabIndex = 4;
            this.cornerUsernamelabel.Text = "Username";
            // 
            // backToHomePageButton
            // 
            this.backToHomePageButton.Location = new System.Drawing.Point(11, 38);
            this.backToHomePageButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.backToHomePageButton.Name = "backToHomePageButton";
            this.backToHomePageButton.Size = new System.Drawing.Size(70, 35);
            this.backToHomePageButton.TabIndex = 5;
            this.backToHomePageButton.Text = "Back to Home page";
            this.backToHomePageButton.UseVisualStyleBackColor = true;
            this.backToHomePageButton.Click += new System.EventHandler(this.backToHomePageButton_Click);
            // 
            // stockManagementLabel
            // 
            this.stockManagementLabel.AutoSize = true;
            this.stockManagementLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stockManagementLabel.Location = new System.Drawing.Point(191, 100);
            this.stockManagementLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.stockManagementLabel.Name = "stockManagementLabel";
            this.stockManagementLabel.Size = new System.Drawing.Size(145, 17);
            this.stockManagementLabel.TabIndex = 6;
            this.stockManagementLabel.Text = "Stock Management";
            // 
            // addStockButton
            // 
            this.addStockButton.Location = new System.Drawing.Point(155, 143);
            this.addStockButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.addStockButton.Name = "addStockButton";
            this.addStockButton.Size = new System.Drawing.Size(71, 29);
            this.addStockButton.TabIndex = 7;
            this.addStockButton.Text = "Add Stock";
            this.addStockButton.UseVisualStyleBackColor = true;
            this.addStockButton.Click += new System.EventHandler(this.addStockButton_Click);
            // 
            // editButton
            // 
            this.editButton.Location = new System.Drawing.Point(281, 143);
            this.editButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.editButton.Name = "editButton";
            this.editButton.Size = new System.Drawing.Size(71, 29);
            this.editButton.TabIndex = 8;
            this.editButton.Text = "Edit Stock";
            this.editButton.UseVisualStyleBackColor = true;
            // 
            // availableStockLabel
            // 
            this.availableStockLabel.AutoSize = true;
            this.availableStockLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.availableStockLabel.Location = new System.Drawing.Point(11, 196);
            this.availableStockLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.availableStockLabel.Name = "availableStockLabel";
            this.availableStockLabel.Size = new System.Drawing.Size(115, 17);
            this.availableStockLabel.TabIndex = 9;
            this.availableStockLabel.Text = "Available Stocks:";
            // 
            // bookNameLabel
            // 
            this.bookNameLabel.AutoSize = true;
            this.bookNameLabel.Location = new System.Drawing.Point(39, 237);
            this.bookNameLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.bookNameLabel.Name = "bookNameLabel";
            this.bookNameLabel.Size = new System.Drawing.Size(63, 13);
            this.bookNameLabel.TabIndex = 10;
            this.bookNameLabel.Text = "Book Name";
            // 
            // availabilityLabel
            // 
            this.availabilityLabel.AutoSize = true;
            this.availabilityLabel.Location = new System.Drawing.Point(172, 237);
            this.availabilityLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.availabilityLabel.Name = "availabilityLabel";
            this.availabilityLabel.Size = new System.Drawing.Size(50, 13);
            this.availabilityLabel.TabIndex = 11;
            this.availabilityLabel.Text = "Availbility";
            // 
            // priceLabel
            // 
            this.priceLabel.AutoSize = true;
            this.priceLabel.Location = new System.Drawing.Point(294, 237);
            this.priceLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.priceLabel.Name = "priceLabel";
            this.priceLabel.Size = new System.Drawing.Size(31, 13);
            this.priceLabel.TabIndex = 12;
            this.priceLabel.Text = "Price";
            // 
            // locationLabel
            // 
            this.locationLabel.AutoSize = true;
            this.locationLabel.Location = new System.Drawing.Point(391, 237);
            this.locationLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.locationLabel.Name = "locationLabel";
            this.locationLabel.Size = new System.Drawing.Size(48, 13);
            this.locationLabel.TabIndex = 13;
            this.locationLabel.Text = "Location";
            // 
            // stockManagementListBox
            // 
            this.stockManagementListBox.FormattingEnabled = true;
            this.stockManagementListBox.Location = new System.Drawing.Point(41, 260);
            this.stockManagementListBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.stockManagementListBox.Name = "stockManagementListBox";
            this.stockManagementListBox.ScrollAlwaysVisible = true;
            this.stockManagementListBox.Size = new System.Drawing.Size(397, 121);
            this.stockManagementListBox.TabIndex = 14;
            // 
            // updateButton
            // 
            this.updateButton.Location = new System.Drawing.Point(209, 393);
            this.updateButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.updateButton.Name = "updateButton";
            this.updateButton.Size = new System.Drawing.Size(70, 25);
            this.updateButton.TabIndex = 15;
            this.updateButton.Text = "Update";
            this.updateButton.UseVisualStyleBackColor = true;
            this.updateButton.Click += new System.EventHandler(this.updateButton_Click);
            // 
            // StockManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(504, 436);
            this.Controls.Add(this.updateButton);
            this.Controls.Add(this.stockManagementListBox);
            this.Controls.Add(this.locationLabel);
            this.Controls.Add(this.priceLabel);
            this.Controls.Add(this.availabilityLabel);
            this.Controls.Add(this.bookNameLabel);
            this.Controls.Add(this.availableStockLabel);
            this.Controls.Add(this.editButton);
            this.Controls.Add(this.addStockButton);
            this.Controls.Add(this.stockManagementLabel);
            this.Controls.Add(this.backToHomePageButton);
            this.Controls.Add(this.cornerUsernamelabel);
            this.Controls.Add(this.LogoImage);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "StockManagement";
            this.Text = "Stock Management";
            ((System.ComponentModel.ISupportInitialize)(this.LogoImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox LogoImage;
        private System.Windows.Forms.Label cornerUsernamelabel;
        private System.Windows.Forms.Button backToHomePageButton;
        private System.Windows.Forms.Label stockManagementLabel;
        private System.Windows.Forms.Button addStockButton;
        private System.Windows.Forms.Button editButton;
        private System.Windows.Forms.Label availableStockLabel;
        private System.Windows.Forms.Label bookNameLabel;
        private System.Windows.Forms.Label availabilityLabel;
        private System.Windows.Forms.Label priceLabel;
        private System.Windows.Forms.Label locationLabel;
        private System.Windows.Forms.ListBox stockManagementListBox;
        private System.Windows.Forms.Button updateButton;
    }
}