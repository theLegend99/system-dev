﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SysDev
{
    public partial class SignIn : Form
    {
        //public static String Username;
        //public static String PassWord;
        public SignIn()
        {
            InitializeComponent();
        }

        private void signUpButton_Click(object sender, EventArgs e)
        {
            Creation createAccount = new Creation();

            createAccount.Show(); // Shows createccount
        }

        private void signInButton_Click(object sender, EventArgs e)
        {
          
             string query = "Select * from AccountList WHERE FirstName = @FirstName AND PassWord = @PassWord";
             SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|AccountList.mdf;Integrated Security=True;Connect Timeout=30");
             SqlCommand cmd = new SqlCommand(query, con);
         
            using (con)
            {
                cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@FirstName", userNameTextBox.Text);
                cmd.Parameters.AddWithValue("@PassWord", passwordTextBox.Text);
                con.Open();
                using (SqlDataReader oReader = cmd.ExecuteReader())
                {
                    while (oReader.Read())
                    {
                        if(userNameTextBox.Text.Equals(oReader["FirstName"].ToString()) && passwordTextBox.Text.Equals(oReader["PassWord"].ToString())){
                            HomePage homePage = new HomePage();
                            homePage.Show();
                        }

                    }
                    con.Close();
                }
            }
        
    }
    }
}
